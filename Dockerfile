FROM rust:latest as builder


WORKDIR /usr/src/individual2


COPY . .


RUN cargo build --release


FROM debian:bookworm-slim



COPY --from=builder /usr/src/individual2/target/release/individual2 .


RUN apt-get update \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*


EXPOSE 8080


CMD ["./individual2"]
