use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;

#[derive(Deserialize)]
struct PrimeQuery {
    number: Option<u32>,
}

async fn check_prime(query: web::Query<PrimeQuery>) -> impl Responder {
    match query.number {
        Some(number) => {
            if is_prime(number) {
                HttpResponse::Ok().body(format!("{} is a prime number", number))
            } else {
                HttpResponse::Ok().body(format!("{} is not a prime number", number))
            }
        },
        None => HttpResponse::BadRequest().body("Wrong arguments!"),
    }
}

fn is_prime(n: u32) -> bool {
    if n <= 1 {
        return false;
    }
    for i in 2..=(n as f64).sqrt() as u32 {
        if n % i == 0 {
            return false;
        }
    }
    true
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/", web::get().to(check_prime)))
    .bind("0.0.0.0:8080")?
    .run()
    .await
}