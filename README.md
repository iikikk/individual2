# Individual project2

## Gitlab repo:
https://gitlab.com/iikikk/individual2
## Demo video:
https://gitlab.com/iikikk/individual2/-/blob/main/individual2.mkv?ref_type=heads


## Project Description
In this project, I will be developing a Rust-based microservice that will provide an interface to the outside world via a simple REST API/web service. Part of the project will include creating a Dockerfile to containerise the service to ensure its consistency and ease of deployment across different environments. In addition, I will configure a CI/CD (Continuous Integration/Continuous Deployment) process to ensure that every update to the code is automatically deployed to the production environment, improving development efficiency and code quality. The final deliverables of the project will include detailed documentation and a demo video showing the functionality and deployment of the microservices!

## Requirements
-	Simple REST API/web service in Rust
-	Dockerfile to containerize service
-	CI/CD pipeline files

## Steps

### Simple REST API/web service in Rust
The main function of my rust software is to determine if the input number is a prime number, and if it is, it outputs "X is a prime number", and if it is not, it outputs "Wrong arguments!".
1. use cargo new <YOUR PROJECT NAME> to create a new rust project.
2. To build a web application, write your own function. Don't forget to include use actix_files:: and use actix_web::{web, App, HttpResponse, HttpServer, Responder}.files layered over one another.
3. create a new index.html under this folder. 
4. In Cargo.toml, include dependencies: actix-web, actix-files, rand

### Dockerfile to containerize service
5. Make a matching Makefile and Dockerfile.
6. run cargo build and cargo run. Open another terminal in the same time, run docker build -t mini4 . You can open web app under http://localhost:8080/

### CI/CD pipeline files
Enable the CI/CD pipeline by creating the .gitlab-ci.yml file.



## Screenshot:

### Rust Function
 ![Alt text](image.png)
 ![Alt text](image-1.png)

### Docker Container
 ![Alt text](image-2.png)

### CI/CD pipeline
  ![Alt text](image-3.png)
